# MoSimPa's monitor

This repository contains [MoSimPa](http://mosimpa.gitlab.io/)'s mqtt+parser library.

It makes use of [GIT flow](https://nvie.com/posts/a-successful-git-branching-model/), so remember:

- master is the production branch.
- develop is the actual development branch from which you should start working
  from.

Happy hacking!
