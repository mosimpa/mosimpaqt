# For more info read:
# Qt5: http://www.kdab.com/using-cmake-with-qt-5/
# Qt4: http://developer.qt.nokia.com/quarterly/view/using_cmake_to_build_qt_projects
PROJECT(mosimpaqt)

# Versión mínima de CMake a utilizar (Debian Buster):
cmake_minimum_required(VERSION 3.13)

# If available let CMake process generated files with automoc/autouic.
cmake_policy(SET CMP0071 NEW)

set(CMAKE_VERBOSE_MAKEFILE on)

# Set the c++17 standard, we will use [[nodiscard]]
set(CMAKE_CXX_STANDARD 17)

# Enable tests.
enable_testing()

#### Versioning ####

# Application version.
set(MOSIMPAQT_MAJOR_VERSION "0")
set(MOSIMPAQT_MINOR_VERSION "0")
set(MOSIMPAQT_PATCH_VERSION "6")
# Release/beta candidate, keep empty on official versions.
set(MOSIMPAQT_RC_VERSION "")

set(MOSIMPAQT_VERSION "${MOSIMPAQT_MAJOR_VERSION}.${MOSIMPAQT_MINOR_VERSION}.${MOSIMPAQT_PATCH_VERSION}${MOSIMPAQT_RC_VERSION}")
set(MOSIMPAQT_VERSION_STRING "${MOSIMPAQT_VERSION}")

# If git is installed add the revision to MOSIMPAQT_VERSION_STRING.
# This is useful to detect development builds, as final builds will not have
# git available.
if(EXISTS "${parser_SOURCE_DIR}/.git")
  find_program(GIT_EXECUTABLE NAMES git)
  if(GIT_EXECUTABLE)
    message(STATUS "Found git: ${GIT_EXECUTABLE}")
    execute_process(COMMAND ${GIT_EXECUTABLE} describe
                    WORKING_DIRECTORY ${parser_SOURCE_DIR}
                    OUTPUT_VARIABLE MOSIMPAQT_GIT_DESCRIBE)
    string(REGEX REPLACE "\n" "" MOSIMPAQT_GIT_DESCRIBE "${MOSIMPAQT_GIT_DESCRIBE}")
    string(REGEX REPLACE "${MOSIMPAQT_VERSION}-" "" MOSIMPAQT_GIT_DESCRIBE "${MOSIMPAQT_GIT_DESCRIBE}")
    set(MOSIMPAQT_GIT_DESCRIBE ${MOSIMPAQT_GIT_DESCRIBE})
    set(MOSIMPAQT_VERSION_STRING "${MOSIMPAQT_VERSION_STRING} revision ${MOSIMPAQT_GIT_DESCRIBE}")
  endif()
endif()

# Create the definitions.
add_definitions(
    -DMOSIMPAQT_MAJOR_VERSION="${MOSIMPAQT_MAJOR_VERSION}"
    -DMOSIMPAQT_MINOR_VERSION="${MOSIMPAQT_MINOR_VERSION}"
    -DMOSIMPAQT_PATCH_VERSION="${MOSIMPAQT_PATCH_VERSION}"
    -DMOSIMPAQT_RC_VERSION="${MOSIMPAQT_RC_VERSION}"
    -DMOSIMPAQT_GIT_DESCRIBE="${MOSIMPAQT_GIT_DESCRIBE}"
    -DMOSIMPAQT_VERSION="${MOSIMPAQT_VERSION}"
    -DMOSIMPAQT_VERSION_STRING="${MOSIMPAQT_VERSION_STRING}"
)

# Log the versions.
message(STATUS "mosimpaqt version: ${MOSIMPAQT_VERSION}")
message(STATUS "Git describe: ${MOSIMPAQT_GIT_DESCRIBE}")

#### Variables to define ####

# Require Qt 5.
find_package(Qt5Core REQUIRED)

# Mosquitto
find_package(PkgConfig REQUIRED)
pkg_check_modules(MOSQUITTO REQUIRED IMPORTED_TARGET libmosquitto)

include(GNUInstallDirs)

message(STATUS "CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}")

# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)
# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Various compilation warnings. Help with correct development.
# More info: http://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html
if (CMAKE_COMPILER_IS_GNUCXX)
    ADD_DEFINITIONS(
        # Warnings for common stuff.
        -Wall
        # Undefined symbols.
        # http://ozlabs.org/~rusty/index.cgi/tech/2008-01-04.html
        -Wundef
        # Cast alignment.
        -Wcast-align
        # Warn on signed char subscripts.
        -Wchar-subscripts
	# Warn if something depends on sizeof.
        -Wpointer-arith
	# Warn on structs marked as packed but with no real effect.
        -Wpacked
        -Wmissing-format-attribute
        -Woverloaded-virtual -Wnon-virtual-dtor
        # Make all warnings an error.
        #-Werror
    )
endif(CMAKE_COMPILER_IS_GNUCXX)

#### Doxygen ####
# Thanks http://majewsky.wordpress.com/2010/08/14/tip-of-the-day-cmake-and-doxygen/

# Agrego un target para generar la documentación con Doxygen.
# Target for generating documentation with Doxygen.
find_package(Doxygen)
if(DOXYGEN_FOUND)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in
        ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY
    )

    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doc/main.dox.in
        ${CMAKE_CURRENT_BINARY_DIR}/dox/main.dox @ONLY
    )

    add_custom_target(doc ${DOXYGEN_EXECUTABLE}
        ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen" VERBATIM
    )

    # Mark the install target as OPTIONAL so it won't fail if doc has not been generated.
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/doc/ DESTINATION share/mosimpaqt/doc/ OPTIONAL)
endif(DOXYGEN_FOUND)

# uninstall target
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)

add_subdirectory(src)
add_subdirectory(tests)
