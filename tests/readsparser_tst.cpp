#include <QString>

#include "../src/readsparser.h"
#include "../src/definitions.h"

#include "mosquittomessage.h"

#include "readsparser_tst.h"

void ReadsParserTests::checkTopic()
{
    QVERIFY2(ReadsParser::topic() == QStringLiteral("reads/#"), "Topic must match.");
}

void ReadsParserTests::checkJsonFormat_data()
{
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<bool>("expectedRetval");

    QTest::addRow("invalid_json") << newMosquittoMessage("001122334455", "foo") << false;
    QTest::addRow("valid_json_no_data") << newMosquittoMessage("001122334455", "{\"foo\":\"var\"}") << false;

    QString payload;
    payload = QString("{\"spo2\":[{\"time\":2,\"SpO2\":950,\"R\":950}],"
                      "\"bloodP\":[{\"time\":2,\"sys\":130,\"dia\":90}],"
                      "\"heartR\":[{\"time\":2,\"heartR\":900,\"HR_AR\":true}],"
                      "\"bodyT\":[{\"time\":2,\"temp\":9475}]}");
    QTest::addRow("valid_json") << newMosquittoMessage("001122334455", payload) << true;
    QTest::addRow("invalid_mac") << newMosquittoMessage("foo", payload) << false;
}

void ReadsParserTests::checkJsonFormat()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, expectedRetval);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    ReadsParser readsParser;
    bool retval = readsParser.parseMessage(&(msg->msg));

    QVERIFY2(retval == expectedRetval, "retval and expectedRetval must match.");
}

void ReadsParserTests::checkSpo2Values_data()
{
    const QString MAC("001122334455");
    const int VALID_TIME = 2;
    const spo2_t MEDIUM_SPO2 = (WIRE_SPO2_MAX + WIRE_SPO2_MIN) / 2;
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<int>("numberOfValues");
    QTest::addColumn<QVector<ReadsParser::SpO2Values>>("expectedValues");

    QString payload;
    QVector<ReadsParser::SpO2Values> expectedValues;
    ReadsParser::SpO2Values values;
    values.time = VALID_TIME;
    values.spO2 = MEDIUM_SPO2;

    // Valid values.
    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2}]}")
                     .arg(values.time).arg(values.spO2);
    expectedValues << values;

    QTest::addRow("valid_spo2_values") << newMosquittoMessage(MAC, payload)
                                       << true
                                       << 1
                                       <<  expectedValues;

    // Wrong time.
    payload = QString("{\"spo2\":[{\"time\":-1,\"SpO2\":%1}]}")
                     .arg(values.spO2);
    QTest::addRow("spo2_wrong_time") << newMosquittoMessage(MAC, payload)
                                     << false
                                     << 0
                                     << expectedValues;

    // SpO2 below minimum.
    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2}]}")
                     .arg(values.time).arg(WIRE_SPO2_MIN - 1);

    QTest::addRow("spo2_min-1") << newMosquittoMessage(MAC, payload)
                                 << false
                                 << 0
                                 << expectedValues;

    // SpO2 minimum.
    values.spO2 = WIRE_SPO2_MIN;
    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2}]}")
                     .arg(values.time).arg(values.spO2);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("spo2_min") << newMosquittoMessage(MAC, payload)
                              << true
                              << 1
                              <<  expectedValues;

    // SpO2 maximum.
    values.spO2 = WIRE_SPO2_MAX;
    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2}]}")
                     .arg(values.time).arg(values.spO2);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("spo2_max") << newMosquittoMessage(MAC, payload)
                              << true
                              << 1
                              <<  expectedValues;

    // SpO2 above maximum.
    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2}]}")
                     .arg(values.time).arg(WIRE_SPO2_MAX + 1);

    QTest::addRow("spo2_max+1") << newMosquittoMessage(MAC, payload)
                              << false
                              << 1
                              <<  expectedValues;

    // Multiple SpO2 values.
    values.time = VALID_TIME;
    values.spO2 = MEDIUM_SPO2;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    values.time = VALID_TIME + 2;
    expectedValues << values;

    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2},"
                      "{\"time\":%4,\"SpO2\":%2},"
                      "{\"time\":%5,\"SpO2\":%2}]}")
                     .arg(VALID_TIME).arg(values.spO2)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2);


    QTest::addRow("multiple_spo2_values") << newMosquittoMessage(MAC, payload)
                                          << true
                                          << 3
                                          << expectedValues;

    // Multiple SpO2 values, one invalid. The last one to simplify testing.
    values.time = VALID_TIME;
    values.spO2 = MEDIUM_SPO2;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    payload = QString("{\"spo2\":[{\"time\":%1,\"SpO2\":%2},"
                      "{\"time\":%4,\"SpO2\":%2},"
                      "{\"time\":%5,\"SpO2\":%6}]}")
                     .arg(VALID_TIME).arg(values.spO2)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2).arg(WIRE_SPO2_MAX+1);


    QTest::addRow("multiple_spo2_values") << newMosquittoMessage(MAC, payload)
                                          << true
                                          << 2
                                          << expectedValues;
}

void ReadsParserTests::checkSpo2Values()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(int, numberOfValues);
    QFETCH(QVector<ReadsParser::SpO2Values>, expectedValues);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    ReadsParser readsParser;
    bool retval = readsParser.parseMessage(&(msg->msg));

    QVERIFY2(retval == expectedRetval, "retval and expectedRetval must match.");

    if(expectedRetval == false)
        return;

    QVERIFY2(readsParser.spO2ValuesSize() == numberOfValues,
             "The number of received values and the expected number do not match.");

    for(int i = 0; i < numberOfValues; i++)
    {
        ReadsParser::SpO2Values values = readsParser.spO2Values().at(i);
        ReadsParser::SpO2Values expectedValue = expectedValues.at(i);
        QVERIFY2(values.time == expectedValue.time, "Time should match.");
        QVERIFY2(values.spO2 == expectedValue.spO2, "SpO2 should match.");
    }
}

void ReadsParserTests::checkHeartRateValues_data()
{
    const QString MAC("001122334455");
    const int VALID_TIME = 2;
    const heart_rate_t MEDIUM_HEART_RATE = (WIRE_HEART_RATE_MAX + WIRE_HEART_RATE_MIN) / 2;
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<int>("numberOfValues");
    QTest::addColumn<QVector<ReadsParser::HeartRateValues>>("expectedValues");

    QString payload;
    QVector<ReadsParser::HeartRateValues> expectedValues;
    ReadsParser::HeartRateValues values;
    values.time = VALID_TIME;
    values.heartR = MEDIUM_HEART_RATE;

    // Valid values.
    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2}]}")
                     .arg(values.time).arg(values.heartR);
    expectedValues << values;

    QTest::addRow("valid_hr_values") << newMosquittoMessage(MAC, payload)
                                     << true
                                     << 1
                                     <<  expectedValues;

    // Wrong time.
    payload = QString("{\"heartR\":[{\"time\":-1,\"heartR\":%1}]}")
                     .arg(values.heartR);
    QTest::addRow("hr_wrong_time") << newMosquittoMessage(MAC, payload)
                                   << false
                                   << 0
                                   << expectedValues;

    // heartR below minimum.
    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2}]}")
                     .arg(values.time).arg(WIRE_HEART_RATE_MIN - 1);

    QTest::addRow("hr_min-1") << newMosquittoMessage(MAC, payload)
                              << false
                              << 0
                              << expectedValues;

    // heartR minimum.
    values.heartR = WIRE_HEART_RATE_MIN;
    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2}]}")
                     .arg(values.time).arg(values.heartR);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("hr_min") << newMosquittoMessage(MAC, payload)
                            << true
                            << 1
                            <<  expectedValues;

    // heartR maximum.
    values.heartR = WIRE_HEART_RATE_MAX;
    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2}]}")
                     .arg(values.time).arg(values.heartR);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("hr_max") << newMosquittoMessage(MAC, payload)
                            << true
                              << 1
                              <<  expectedValues;

    // heartR above maximum.
    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2}]}")
                     .arg(values.time).arg(WIRE_HEART_RATE_MAX + 1);

    QTest::addRow("hr_max+1") << newMosquittoMessage(MAC, payload)
                              << false
                              << 1
                              <<  expectedValues;

    // Multiple heart rate values.
    values.time = VALID_TIME;
    values.heartR = MEDIUM_HEART_RATE;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    values.time = VALID_TIME + 2;
    expectedValues << values;

    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2},"
                      "{\"time\":%3,\"heartR\":%2},"
                      "{\"time\":%4,\"heartR\":%2}]}")
                     .arg(VALID_TIME).arg(values.heartR)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2);

    QTest::addRow("multiple_hr_values") << newMosquittoMessage(MAC, payload)
                                        << true
                                        << 3
                                        << expectedValues;

    // Multiple heart rate values, one invalid. The last one to simplify testing.
    values.time = VALID_TIME;
    values.heartR = MEDIUM_HEART_RATE;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    payload = QString("{\"heartR\":[{\"time\":%1,\"heartR\":%2},"
                      "{\"time\":%3,\"heartR\":%2},"
                      "{\"time\":%4,\"heartR\":%5}]}")
                     .arg(VALID_TIME).arg(values.heartR)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2).arg(WIRE_HEART_RATE_MAX + 1);

    QTest::addRow("multiple_hr_values") << newMosquittoMessage(MAC, payload)
                                        << true
                                        << 2
                                        << expectedValues;
}

void ReadsParserTests::checkHeartRateValues()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(int, numberOfValues);
    QFETCH(QVector<ReadsParser::HeartRateValues>, expectedValues);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    ReadsParser readsParser;
    bool retval = readsParser.parseMessage(&(msg->msg));

    QVERIFY2(retval == expectedRetval, "retval and expectedRetval must match.");

    if(expectedRetval == false)
        return;

    QVERIFY2(readsParser.heartRateValuesSize() == numberOfValues,
             "The number of received values and the expected number do not match.");

    for(int i = 0; i < numberOfValues; i++)
    {
        ReadsParser::HeartRateValues values = readsParser.heartRateValues().at(i);
        ReadsParser::HeartRateValues expectedValue = expectedValues.at(i);
        QVERIFY2(values.time == expectedValue.time, "Time should match.");
        QVERIFY2(values.heartR == expectedValue.heartR, "Heart rate should match.");
    }
}

void ReadsParserTests::checkBloodPressureValues_data()
{
    const QString MAC("001122334455");
    const int VALID_TIME = 2;
    const blood_pressure_t MEDIUM_BLOOD_PRESSURE_SYS = (WIRE_BLOOD_PRESSURE_SYS_MAX + WIRE_BLOOD_PRESSURE_SYS_MIN) / 2;
    const blood_pressure_t MEDIUM_BLOOD_PRESSURE_DIAS = (WIRE_BLOOD_PRESSURE_DIAS_MAX + WIRE_BLOOD_PRESSURE_DIAS_MIN) / 2;
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<int>("numberOfValues");
    QTest::addColumn<QVector<ReadsParser::BloodPressureValues>>("expectedValues");

    QString payload;
    QVector<ReadsParser::BloodPressureValues> expectedValues;
    ReadsParser::BloodPressureValues values;
    values.time = VALID_TIME;
    values.sys = MEDIUM_BLOOD_PRESSURE_SYS;
    values.dias = MEDIUM_BLOOD_PRESSURE_DIAS;

    // Valid values.
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(values.sys).arg(values.dias);
    expectedValues << values;

    QTest::addRow("valid_bp_values") << newMosquittoMessage(MAC, payload)
                                     << true
                                     << 1
                                     <<  expectedValues;

    // Wrong time.
    payload = QString("{\"bloodP\":[{\"time\":-1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.sys).arg(values.dias);
    QTest::addRow("bp_wrong_time") << newMosquittoMessage(MAC, payload)
                                   << false
                                   << 0
                                   << expectedValues;

    // bloodP sys below minimum.
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(WIRE_BLOOD_PRESSURE_SYS_MIN - 1).arg(values.dias);

    QTest::addRow("bp_sys_min-1") << newMosquittoMessage(MAC, payload)
                                  << false
                                  << 0
                                  << expectedValues;

    // bloodP dias below minimum.
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(values.sys).arg(WIRE_BLOOD_PRESSURE_DIAS_MIN - 1);

    QTest::addRow("bp_dias_min-1") << newMosquittoMessage(MAC, payload)
                                   << false
                                   << 0
                                   << expectedValues;

    // bloodP sys minimum.
    values.sys = WIRE_BLOOD_PRESSURE_SYS_MIN;
    values.dias = MEDIUM_BLOOD_PRESSURE_DIAS;
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(values.sys).arg(values.dias);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("bp_sys_min") << newMosquittoMessage(MAC, payload)
                                << true
                                << 1
                                << expectedValues;

    // bloodP dias minimum.
    values.sys = MEDIUM_BLOOD_PRESSURE_SYS;
    values.dias = WIRE_BLOOD_PRESSURE_DIAS_MIN;
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(values.sys).arg(values.dias);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("bp_dias_min") << newMosquittoMessage(MAC, payload)
                                << true
                                << 1
                                <<  expectedValues;

    // bloodP sys maximum.
    values.sys = WIRE_BLOOD_PRESSURE_SYS_MAX;
    values.dias = MEDIUM_BLOOD_PRESSURE_DIAS;
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(values.sys).arg(values.dias);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("bp_sys_max") << newMosquittoMessage(MAC, payload)
                                << true
                                << 1
                                <<  expectedValues;

    // bloodP dias maximum.
    values.sys = MEDIUM_BLOOD_PRESSURE_SYS;
    values.dias = WIRE_BLOOD_PRESSURE_DIAS_MAX;
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(values.sys).arg(values.dias);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("bp_dias_max") << newMosquittoMessage(MAC, payload)
                                 << true
                                 << 1
                                 << expectedValues;

    // bloodP sys above maximum.
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(WIRE_BLOOD_PRESSURE_SYS_MAX + 1).arg(values.dias);

    QTest::addRow("bp_sys_max+1") << newMosquittoMessage(MAC, payload)
                                  << false
                                  << 1
                                  << expectedValues;

    // bloodP dias above maximum.
    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3}]}")
                     .arg(values.time).arg(MEDIUM_BLOOD_PRESSURE_SYS).arg(WIRE_BLOOD_PRESSURE_DIAS_MAX + 1);

    QTest::addRow("bp_sys_max+1") << newMosquittoMessage(MAC, payload)
                                  << false
                                  << 1
                                  << expectedValues;

    // Multiple bloodP rate values.
    values.time = VALID_TIME;
    values.sys = MEDIUM_BLOOD_PRESSURE_SYS;
    values.dias = MEDIUM_BLOOD_PRESSURE_DIAS;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    values.time = VALID_TIME + 2;
    expectedValues << values;

    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3},"
                      "{\"time\":%4,\"sys\":%2,\"dia\":%3},"
                      "{\"time\":%5,\"sys\":%2,\"dia\":%3}]}")
                     .arg(VALID_TIME).arg(values.sys).arg(values.dias)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2);

    QTest::addRow("multiple_bp_values") << newMosquittoMessage(MAC, payload)
                                        << true
                                        << 3
                                        << expectedValues;

    // Multiple blood pressure values, one invalid. The last one to simplify testing.
    values.time = VALID_TIME;
    values.sys = MEDIUM_BLOOD_PRESSURE_SYS;
    values.dias = MEDIUM_BLOOD_PRESSURE_DIAS;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    payload = QString("{\"bloodP\":[{\"time\":%1,\"sys\":%2,\"dia\":%3},"
                      "{\"time\":%4,\"sys\":%2,\"dia\":%3},"
                      "{\"time\":%5,\"sys\":%6,\"dia\":%3}]}")
                     .arg(VALID_TIME).arg(values.sys).arg(values.dias)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2).arg(WIRE_BLOOD_PRESSURE_SYS_MAX + 1);

    QTest::addRow("multiple_hr_values") << newMosquittoMessage(MAC, payload)
                                        << true
                                        << 2
                                        << expectedValues;
}

void ReadsParserTests::checkBloodPressureValues()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(int, numberOfValues);
    QFETCH(QVector<ReadsParser::BloodPressureValues>, expectedValues);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    ReadsParser readsParser;
    bool retval = readsParser.parseMessage(&(msg->msg));

    QVERIFY2(retval == expectedRetval, "retval and expectedRetval must match.");

    if(expectedRetval == false)
        return;

    QVERIFY2(readsParser.bloodPressureValuesSize() == numberOfValues,
             "The number of received values and the expected number do not match.");

    for(int i = 0; i < numberOfValues; i++)
    {
        ReadsParser::BloodPressureValues values = readsParser.bloodPressureValues().at(i);
        ReadsParser::BloodPressureValues expectedValue = expectedValues.at(i);
        QVERIFY2(values.time == expectedValue.time, "Time should match.");
        QVERIFY2(values.sys == expectedValue.sys, "Blood pressure sys should match.");
        QVERIFY2(values.dias == expectedValue.dias, "Blood pressure dias should match.");
    }
}

void ReadsParserTests::checkBodyTempValues_data()
{
    const QString MAC("001122334455");
    const int VALID_TIME = 2;
    const body_temp_t MEDIUM_BODY_TEMP = (WIRE_BODY_TEMP_MAX + WIRE_BODY_TEMP_MIN) / 2;
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<int>("numberOfValues");
    QTest::addColumn<QVector<ReadsParser::BodyTempValues>>("expectedValues");

    QString payload;
    QVector<ReadsParser::BodyTempValues> expectedValues;
    ReadsParser::BodyTempValues values;
    values.time = VALID_TIME;
    values.bodyTemp = MEDIUM_BODY_TEMP;

    // Valid values.
    payload = QString("{\"bodyT\":[{\"time\":%1,\"temp\":%2}]}")
                     .arg(values.time).arg(values.bodyTemp);
    expectedValues << values;

    QTest::addRow("valid_bt_values") << newMosquittoMessage(MAC, payload)
                                     << true
                                     << 1
                                     << expectedValues;

    // Wrong time.
    payload = QString("{\"bodyTemp\":[{\"time\":-1,\"temp\":%1}]}")
                     .arg(values.bodyTemp);
    QTest::addRow("bt_wrong_time") << newMosquittoMessage(MAC, payload)
                                   << false
                                   << 0
                                   << expectedValues;

    // bodyTemp below minimum.
    payload = QString("{\"bodyTemp\":[{\"time\":%1,\"temp\":%2}]}")
                     .arg(values.time).arg(WIRE_BODY_TEMP_MIN - 1);

    QTest::addRow("bt_min-1") << newMosquittoMessage(MAC, payload)
                              << false
                              << 0
                              << expectedValues;

    // bodyTemp minimum.
    values.bodyTemp = WIRE_BODY_TEMP_MIN;
    payload = QString("{\"bodyT\":[{\"time\":%1,\"temp\":%2}]}")
                     .arg(values.time).arg(values.bodyTemp);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("bt_min") << newMosquittoMessage(MAC, payload)
                            << true
                            << 1
                            << expectedValues;

    // bodyTemp maximum.
    values.bodyTemp = WIRE_BODY_TEMP_MAX;
    payload = QString("{\"bodyT\":[{\"time\":%1,\"temp\":%2}]}")
                     .arg(values.time).arg(values.bodyTemp);
    expectedValues.clear();
    expectedValues << values;

    QTest::addRow("bt_max") << newMosquittoMessage(MAC, payload)
                            << true
                            << 1
                            << expectedValues;

    // bodyTemp above maximum.
    payload = QString("{\"bodyT\":[{\"time\":%1,\"temp\":%2}]}")
                     .arg(values.time).arg(WIRE_BODY_TEMP_MAX + 1);

    QTest::addRow("bt_max+1") << newMosquittoMessage(MAC, payload)
                              << false
                              << 1
                              << expectedValues;

    // Multiple body temperature values.
    values.time = VALID_TIME;
    values.bodyTemp = MEDIUM_BODY_TEMP;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    values.time = VALID_TIME + 2;
    expectedValues << values;

    payload = QString("{\"bodyT\":[{\"time\":%1,\"temp\":%2},"
                      "{\"time\":%3,\"temp\":%2},"
                      "{\"time\":%4,\"temp\":%2}]}")
                     .arg(VALID_TIME).arg(values.bodyTemp)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2);

    QTest::addRow("multiple_bt_values") << newMosquittoMessage(MAC, payload)
                                        << true
                                        << 3
                                        << expectedValues;

    // Multiple body temp values, one invalid. The last one to simplify testing.
    values.time = VALID_TIME;
    values.bodyTemp = MEDIUM_BODY_TEMP;
    expectedValues.clear();
    expectedValues << values;

    values.time = VALID_TIME + 1;
    expectedValues << values;

    payload = QString("{\"bodyT\":[{\"time\":%1,\"temp\":%2},"
                      "{\"time\":%3,\"temp\":%2},"
                      "{\"time\":%4,\"temp\":%5}]}")
                     .arg(VALID_TIME).arg(values.bodyTemp)
                     .arg(VALID_TIME+1)
                     .arg(VALID_TIME+2).arg(WIRE_BODY_TEMP_MAX + 1);

    QTest::addRow("multiple_bt_values") << newMosquittoMessage(MAC, payload)
                                        << true
                                        << 2
                                        << expectedValues;
}

void ReadsParserTests::checkBodyTempValues()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(int, numberOfValues);
    QFETCH(QVector<ReadsParser::BodyTempValues>, expectedValues);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    ReadsParser readsParser;
    bool retval = readsParser.parseMessage(&(msg->msg));

    QVERIFY2(retval == expectedRetval, "retval and expectedRetval must match.");

    if(expectedRetval == false)
        return;

    QVERIFY2(readsParser.bodyTempValuesSize() == numberOfValues,
             "The number of received values and the expected number do not match.");

    for(int i = 0; i < numberOfValues; i++)
    {
        ReadsParser::BodyTempValues values = readsParser.bodyTempValues().at(i);
        ReadsParser::BodyTempValues expectedValue = expectedValues.at(i);
        QVERIFY2(values.time == expectedValue.time, "Time should match.");
        QVERIFY2(values.bodyTemp == expectedValue.bodyTemp, "Body temperature should match.");
    }
}

MosquittoMessage *ReadsParserTests::newMosquittoMessage(const QString & mac, const QString &payload)
{
    return new MosquittoMessage("reads/" + mac, payload, this);
}

QTEST_MAIN(ReadsParserTests)
#include "readsparser_tst.moc"
