#ifndef SCALING_TESTS_H
#define SCALING_TESTS_H

#include <QObject>
#include <QtTest/QTest>

class ScalingTests : public QObject
{
    Q_OBJECT

private slots:
    void checkDeScalings();
    void checkScaling();
};

#endif // SCALING_TESTS_H
