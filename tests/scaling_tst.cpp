#include "../src/scaling.h"

#include "scaling_tst.h"

using namespace MoSimPa;

void ScalingTests::checkDeScalings()
{
    QVERIFY2(qFuzzyCompare(5.0, Scaling::spO2WireToPercentage(50)), "Double must be divided by 10");
    QVERIFY2(qFuzzyCompare(5.0, Scaling::heartRateWireToBPM(50)), "Double must be divided by 10");
    QVERIFY2(qFuzzyCompare(36.5, Scaling::bodyTemperatureWireToDegCelsius(365)), "Double must be divided by 10.0.");
}

void ScalingTests::checkScaling()
{
    QVERIFY2(Scaling::spO2PercentageToWire(5.0) == 50, "Double must be multiplied by 10");
    QVERIFY2(Scaling::heartRateBPMToWire(5.0) == 50, "Double must be multiplied by 10");
    QVERIFY2(Scaling::bodyTemperatureDegCelsiusToWire(36.5) == 365, "Double must be multiplied by 10");
}

QTEST_MAIN(ScalingTests)
#include "scaling_tst.moc"
