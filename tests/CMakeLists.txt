find_package(Qt5Test REQUIRED)

include(GenerateExportHeader)
generate_export_header(${CMAKE_PROJECT_NAME})

# Definitions tests.
add_executable(scalings_tst
  scaling_tst.cpp
)

target_link_libraries(scalings_tst
  Qt5::Test
  Qt5::Core
)

add_test(scalings_tst scalings_tst)

# ReadsParser tests.
add_executable(readsparser_tst
  ../src/readsparser.cpp
  readsparser_tst.cpp
  mosquittomessage.cpp
)

target_link_libraries(readsparser_tst
  Qt5::Test
  Qt5::Core
  PkgConfig::MOSQUITTO
)

add_test(readsparser_tst readsparser_tst)
