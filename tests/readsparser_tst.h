﻿#ifndef READSPARSER_TESTS_H
#define READSPARSER_TESTS_H

#include <QObject>
#include <QtTest/QTest>

#include "../src/readsparser.h"

#include "mosquittomessage.h"

Q_DECLARE_METATYPE(QVector<ReadsParser::SpO2Values>);
Q_DECLARE_METATYPE(QVector<ReadsParser::HeartRateValues>);
Q_DECLARE_METATYPE(QVector<ReadsParser::BloodPressureValues>);
Q_DECLARE_METATYPE(QVector<ReadsParser::BodyTempValues>);

class ReadsParserTests : public QObject
{
    Q_OBJECT

private slots:
    void checkTopic();

    void checkJsonFormat_data();
    void checkJsonFormat();

    void checkSpo2Values_data();
    void checkSpo2Values();

    void checkHeartRateValues_data();
    void checkHeartRateValues();

    void checkBloodPressureValues_data();
    void checkBloodPressureValues();

    void checkBodyTempValues_data();
    void checkBodyTempValues();

private:
    MosquittoMessage * newMosquittoMessage(const QString &mac, const QString & payload);
};

#endif // READSPARSER_TESTS_H
