#include <QObject>
#include <QString>
#include <QByteArray>
#include <QDebug>

#include "mosquittomessage.h"

MosquittoMessage::MosquittoMessage(const QString &topic, const QString &payload, QObject *parent) : QObject(parent)
{
    const int topicSize = topic.size() + 1;
    const int payloadSize = payload.size() + 1;

    msg.mid = 0;
    msg.qos = 0;
    msg.topic = new char[topicSize];
    strcpy(msg.topic, topic.toUtf8().constData());
    msg.retain = false;
    msg.payload = calloc(static_cast<size_t>(payloadSize), sizeof(char));
    strcpy(static_cast<char*>(msg.payload), payload.toUtf8().constData());
    msg.payloadlen = payloadSize - 1;
}

MosquittoMessage::~MosquittoMessage()
{
    if(msg.topic != nullptr)
        delete[] msg.topic;

    free(msg.payload);
}
