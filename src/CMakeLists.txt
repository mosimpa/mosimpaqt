# https://stackoverflow.com/questions/17511496/how-to-create-a-shared-library-with-cmake

include(GenerateExportHeader)

# Be sure to export all symbols in Windows.
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS 1)

set(mosimpa_headers
    definitions.h
    mosquittoclient.h
    readsparser.h
    scaling.h
)

set(mosimpa_sources
    mosquittoclient.cpp
    readsparser.cpp
)

set(LIB_DESCRIPTION "This library holds the Mosquitto client code and related parsers.")

set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_VISIBILITY_INLINES_HIDDEN 1)
add_library(${CMAKE_PROJECT_NAME} SHARED ${mosimpa_sources})

set_target_properties(${CMAKE_PROJECT_NAME}
    PROPERTIES VERSION ${MOSIMPAQT_VERSION}
    SOVERSION ${MOSIMPAQT_MAJOR_VERSION}
    EXPORT_NAME MosimpaQt
)

target_link_libraries(${CMAKE_PROJECT_NAME}
    PUBLIC
    Qt5::Core
    PkgConfig::MOSQUITTO
)

target_include_directories(${CMAKE_PROJECT_NAME}
    PUBLIC
    $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/>
    PRIVATE .
)

generate_export_header(${CMAKE_PROJECT_NAME})

install(TARGETS ${CMAKE_PROJECT_NAME} EXPORT MosimpaQtConfig
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
)

install(
    FILES ${CMAKE_CURRENT_SOURCE_DIR}/${mosimpa_headers}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
)
install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/mosimpaqt_export.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
)

install(
    EXPORT MosimpaQtConfig
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

# Create a pkg-config file
configure_file(mosimpaqt.pc.in mosimpaqt.pc @ONLY)
install(
    FILES ${CMAKE_BINARY_DIR}/src/mosimpaqt.pc
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
)
