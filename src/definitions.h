#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <stdint.h>
#include <time.h>
#include <limits>

namespace MoSimPa {
  // Unscaled values. In other words, in the wire values.
  typedef int32_t db_id_t;
  typedef uint16_t spo2_t;
  typedef uint16_t heart_rate_t;
  typedef int16_t body_temp_t;
  typedef uint8_t blood_pressure_t;
  typedef int32_t db_time_t;

  // Devices info.
  typedef time_t time_t;
  typedef uint16_t batt_mv_t;
  typedef uint32_t msg_id_t;

  /*
   * PostgreSQL's serial range per
   * https://www.postgresql.org/docs/11/datatype-numeric.html
   */
  const db_id_t DB_ID_MIN = 1;
  const db_id_t DB_ID_MAX = 0x7FFFFFFF;

  /*
   * Ranges per https://mosimpa.gitlab.io/requerimientos/#rangos-de-los-valores-medidos
   */
  const spo2_t WIRE_SPO2_MIN = 0;
  const spo2_t WIRE_SPO2_MAX = 1000;

  const heart_rate_t WIRE_HEART_RATE_MIN = 0;
  const heart_rate_t WIRE_HEART_RATE_MAX = 3000;

  const blood_pressure_t WIRE_BLOOD_PRESSURE_SYS_MIN = 0;
  const blood_pressure_t WIRE_BLOOD_PRESSURE_SYS_MAX = std::numeric_limits<blood_pressure_t>::max();

  const blood_pressure_t WIRE_BLOOD_PRESSURE_DIAS_MIN = 0;
  const blood_pressure_t WIRE_BLOOD_PRESSURE_DIAS_MAX = std::numeric_limits<blood_pressure_t>::max();

  // Minimum: 0.
  const body_temp_t WIRE_BODY_TEMP_MIN = 0;
  // 500 == 50ºC
  const body_temp_t WIRE_BODY_TEMP_MAX = 500;
}

#endif // DEFINITIONS_H
