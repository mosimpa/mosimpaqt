/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QDebug>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QByteArray>
#include <QRegExp>

#include "mosquittoclient.h"
#include "readsparser.h"

ReadsParser::ReadsParser()
{
}

/**
 * @brief ReadsParser::parseMessage
 * @param msg message to be parsed.
 * @return false if the data can't be parsed, the MAC is invalid or no data was
 * found. True otherwise.
 *
 * Internal data will be whipped out whenever this method is called.
 */
bool ReadsParser::parseMessage(const mosquitto_message * msg)
{
    QRegExp macRx("^([0-9a-f]{2}){6}$");
    Q_ASSERT_X(macRx.isValid(), __FUNCTION__, "The MAC regular expression must be valid.");

    _mac = QString(msg->topic).remove("reads/");
    if(!macRx.exactMatch(_mac))
        return false;

    _spo2Values.clear();
    _heartRateValues.clear();
    _bloodPressureValues.clear();
    _bodyTempValues.clear();

    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return false;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(mainObject.contains("spo2") && mainObject["spo2"].isArray())
        parseSpO2(mainObject["spo2"].toArray());

    if(mainObject.contains("bloodP") && mainObject["bloodP"].isArray())
        parseBloodPressure(mainObject["bloodP"].toArray());

    if(mainObject.contains("heartR") && mainObject["heartR"].isArray())
        parseHeartRate(mainObject["heartR"].toArray());

    if(mainObject.contains("bodyT") && mainObject["bodyT"].isArray())
        parseBodyTemp(mainObject["bodyT"].toArray());

    if(_spo2Values.isEmpty() && _heartRateValues.isEmpty() &&
       _bloodPressureValues.isEmpty() && _bodyTempValues.isEmpty())
        return false;

    return true;
}

void ReadsParser::parseSpO2(const QJsonArray &spo2Object)
{
    for(int i = 0; i < spo2Object.size(); i++)
    {
        int64_t tmp;
        QJsonObject data = spo2Object[i].toObject();
        struct SpO2Values values;

        // Time.
        if(!data.contains("time") || !data["time"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["time"].toDouble(-1));
        if(tmp < 0)
            continue;

        values.time = static_cast<time_t>(tmp);

        // SpO2.
        if(!data.contains("SpO2") || !data["SpO2"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["SpO2"].toDouble(WIRE_SPO2_MIN - 1));
        if((tmp < WIRE_SPO2_MIN) || (tmp > WIRE_SPO2_MAX))
            continue;

        values.spO2 = static_cast<spo2_t>(tmp);

        _spo2Values.append(values);
    }
}

void ReadsParser::parseBloodPressure(const QJsonArray &bloodPObject)
{
    for(int i = 0; i < bloodPObject.size(); i++)
    {
        int64_t tmp;
        QJsonObject data = bloodPObject[i].toObject();
        struct BloodPressureValues values;

        // Time.
        if(!data.contains("time") || !data["time"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["time"].toDouble(-1));
        if((tmp < 0) || (tmp > std::numeric_limits<time_t>::max()))
            continue;

        values.time = static_cast<time_t>(tmp);

        // Systolic
        if(!data.contains("sys") || !data["sys"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["sys"].toDouble(WIRE_BLOOD_PRESSURE_SYS_MIN - 1));
        if((tmp < WIRE_BLOOD_PRESSURE_SYS_MIN) || (tmp > WIRE_BLOOD_PRESSURE_SYS_MAX))
            continue;

        values.sys = static_cast<blood_pressure_t>(tmp);

        // Diastolic
        if(!data.contains("dia") || !data["dia"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["dia"].toDouble(WIRE_BLOOD_PRESSURE_DIAS_MIN - 1));
        if((tmp < WIRE_BLOOD_PRESSURE_DIAS_MIN) || (tmp > WIRE_BLOOD_PRESSURE_DIAS_MAX))
            continue;

        values.dias = static_cast<blood_pressure_t>(tmp);

        _bloodPressureValues.append(values);
    }
}

void ReadsParser::parseHeartRate(const QJsonArray &heartRateObject)
{
    for(int i = 0; i < heartRateObject.size(); i++)
    {
        int64_t tmp;
        QJsonObject data = heartRateObject[i].toObject();
        struct HeartRateValues values;

        // Time.
        if(!data.contains("time") || !data["time"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["time"].toDouble(-1));
        if((tmp < 0) || (tmp > std::numeric_limits<time_t>::max()))
            continue;

        values.time = static_cast<time_t>(tmp);

        // Heart rate.
        if(!data.contains("heartR") || !data["heartR"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["heartR"].toDouble(WIRE_HEART_RATE_MIN - 1));
        if((tmp < WIRE_HEART_RATE_MIN) || (tmp > WIRE_HEART_RATE_MAX))
            continue;

        values.heartR = static_cast<heart_rate_t>(tmp);

        _heartRateValues.append(values);
    }
}

void ReadsParser::parseBodyTemp(const QJsonArray &bodyTempObject)
{
    for(int i = 0; i < bodyTempObject.size(); i++)
    {
        int64_t tmp;
        QJsonObject data = bodyTempObject[i].toObject();
        struct BodyTempValues values;

        // Time.
        if(!data.contains("time") || !data["time"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["time"].toDouble(-1));
        if((tmp < 0) || (tmp > std::numeric_limits<time_t>::max()))
            continue;

        values.time = static_cast<time_t>(tmp);

        // Temperature.
        if(!data.contains("temp") || !data["temp"].isDouble())
            continue;

        tmp = static_cast<int64_t>(data["temp"].toDouble(WIRE_BODY_TEMP_MIN - 1));
        if((tmp < WIRE_BODY_TEMP_MIN) || (tmp > WIRE_BODY_TEMP_MAX))
            continue;

        values.bodyTemp = static_cast<body_temp_t>(tmp);

        _bodyTempValues.append(values);
    }
}
