/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QObject>
#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QMutex>
#include <QMutexLocker>

#include <mosquitto.h>

#include "mosquittoclient.h"

/**
 * @brief MosquittoClient::MosquittoClient
 * @param logEnabled
 * @param parent
 *
 * The client will autoreconnect and instruct the MQTT broker to unsubscribe
 * the client from all topics upon disconnection.
 */
MosquittoClient::MosquittoClient(bool logEnabled, QObject *parent) : QObject(parent)
{
    mLogEnabled = logEnabled;
    mIsRunning = false;
    mosquitto_lib_init();

    /*
     * - Do not provide a default client ID.
     * - Set set clean_session to true in order to instruct the broker to clean
     *   up subscriptions upon disconnect. This is required if ID is NULL, but
     *   we also want thie behavior.
     * - Pass a pointer to this class as obj, this help us to code callbacks.
     */
    mMosq = mosquitto_new(nullptr, true, this);
    mosquitto_log_callback_set(mMosq, &MosquittoClient::trampLogCallback);
    mosquitto_connect_callback_set(mMosq, &MosquittoClient::trampConnectCallback);
    mosquitto_message_callback_set(mMosq, &MosquittoClient::trampMessageCallback);
    mosquitto_subscribe_callback_set(mMosq, &MosquittoClient::trampSubscribeCallback);
}

MosquittoClient::~MosquittoClient()
{
    mosquitto_disconnect(mMosq);
    mosquitto_loop_stop(mMosq, true);
    mosquitto_destroy(mMosq);
    mosquitto_lib_cleanup();
}

/**
 * @brief MosquittoClient::tlsSet
 * See mosquitto.h's mosquitto_tls_set for the explanation of the API.
 */
int MosquittoClient::tlsSet(const QString &caFile, const QString &caPath, const QString &certFile, const QString &keyFile)
{
    const char * cafile = caFile.isEmpty() ? nullptr : caFile.toUtf8().constData();
    const char * capath = caPath.isEmpty() ? nullptr : caPath.toUtf8().constData();
    const char * certfile = certFile.isEmpty() ? nullptr : certFile.toUtf8().constData();
    const char * keyfile = keyFile.isEmpty() ? nullptr : keyFile.toUtf8().constData();

    return mosquitto_tls_set(mMosq, cafile, capath, certfile, keyfile, nullptr);
}

int MosquittoClient::connect(const QString & host, const int port, const int keepAlive)
{
    if(mIsRunning)
        return MOSQ_ERR_SUCCESS;

    int retval = mosquitto_connect_async(mMosq, host.toLatin1(), port, keepAlive);

    /// \todo If retval != MOSQ_ERR_SUCCESS then grab the error message. Or provide API for that.

    readData();

    mIsRunning = true;

    return retval;
}

int MosquittoClient::publish(const QString topic, const QByteArray payload, int qos, bool retain)
{
    return mosquitto_publish(mMosq, nullptr, topic.toUtf8(), payload.size(), payload.constData(), qos, retain);
}

int MosquittoClient::subscribe(const QString subscriptionPattern, const int qos)
{
    return mosquitto_subscribe(mMosq, nullptr, subscriptionPattern.toUtf8(), qos);
}

int MosquittoClient::unsubscribe(const QString &unsubscriptionPattern)
{
    return mosquitto_unsubscribe(mMosq, nullptr, unsubscriptionPattern.toUtf8());
}

/**
 * @brief MosquittoClient::messages
 * @return A vector with pointers to struct mosquitto_msg. The caller must free
 * them after use.
 */
QVector<mosquitto_message *> MosquittoClient::messages()
{
    QMutexLocker locker(&mMsgsMutex);
    QVector<struct mosquitto_message*> retval = mMsgs;
    mMsgs.clear();
    return retval;
}

void MosquittoClient::trampLogCallback(mosquitto *mosq, void *userdata, int level, const char *str)
{
    if(userdata != nullptr)
        static_cast<MosquittoClient*>(userdata)->logCallback(mosq, userdata, level, str);
}

void MosquittoClient::trampConnectCallback(mosquitto *mosq, void *userdata, int result)
{
    if(userdata != nullptr)
        static_cast<MosquittoClient*>(userdata)->connectCallback(mosq, userdata, result);
}

void MosquittoClient::trampMessageCallback(mosquitto * mosq, void * userdata, const mosquitto_message * msg)
{
    if(userdata != nullptr)
        static_cast<MosquittoClient*>(userdata)->messageCallback(mosq, userdata, msg);
}

void MosquittoClient::trampSubscribeCallback(mosquitto *mosq, void *userdata, int mId, int qosCount, const int *grantedQos)
{
    if(userdata != nullptr)
        static_cast<MosquittoClient*>(userdata)->subscribeCallback(mosq, userdata, mId, qosCount, grantedQos);
}

void MosquittoClient::logCallback(mosquitto *mosq, void *userdata, int level, const char *str)
{
    Q_UNUSED(mosq)
    Q_UNUSED(userdata)

    if(!mLogEnabled)
        return;

    qDebug() << "Log callback, level: " << level << " String: " << str;
}

void MosquittoClient::connectCallback(mosquitto *mosq, void *userdata, int result)
{
    Q_UNUSED(mosq)
    Q_UNUSED(userdata)

    emit connectionTried(result);

    qDebug() << "Connection tried with result " << result;
}

void MosquittoClient::messageCallback(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *msg)
{
    Q_UNUSED(mosq)
    Q_UNUSED(userdata)

    if(msg == nullptr)
    {
        qWarning() << "Received null message.";
        return;
    }

    struct mosquitto_message * newMsg = new struct mosquitto_message();
    if(newMsg == nullptr)
    {
        qWarning() << "Could not create new message struct.";
        return;
    }

    int result = mosquitto_message_copy(newMsg, msg);
    if(result != MOSQ_ERR_SUCCESS)
    {
        qWarning() << "Could not copy MQTT message, result was " << result;
        delete newMsg;
        return;
    }

    QMutexLocker locker(&mMsgsMutex);
    mMsgs.append(newMsg);
    emit messageReceived();
}

void MosquittoClient::subscribeCallback(mosquitto *mosq, void *userdata, int mId, int qosCount, const int *grantedQos)
{
    Q_UNUSED(mosq)
    Q_UNUSED(userdata)

    qDebug() << "Subscribed with message ID " << mId;

    for(int i = 0; i < qosCount; i++)
        qDebug() << "QOS count " << i << " with value " << grantedQos;
}

/**
 * @brief MosquittoClient::readData
 *
 * This method uses mosquitto_loop_start(), which creates a new thread for
 * handling MQTT messages.
 *
 * It will also try to reconnect by itself if disconnected, see
 * mosquitto_reconnect_delay_set's documentation for more information in this.
 */
void MosquittoClient::readData()
{
    int retval = mosquitto_loop_start(mMosq);

    if(retval != MOSQ_ERR_SUCCESS)
    {
        qDebug() << "Error " << retval << " while looping!";
        emit errorWhileLooping(retval);
    }
}
