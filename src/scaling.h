#ifndef SCALING_H
#define SCALING_H

#include <stdint.h>

#include "mosimpaqt_export.h"
#include "definitions.h"

using namespace MoSimPa;

class MOSIMPAQT_EXPORT Scaling {
public:
    static double spO2WireToPercentage(spo2_t wire) { return wire / 10.0; }
    static double heartRateWireToBPM(heart_rate_t wire) { return wire / 10.0; }
    static double bodyTemperatureWireToDegCelsius(body_temp_t wire) { return wire / 10.0; }

    static spo2_t spO2PercentageToWire(double percentage) { return static_cast<spo2_t>(percentage * 10.0); }
    static heart_rate_t heartRateBPMToWire(double bpm) { return static_cast<heart_rate_t>(bpm * 10.0); }
    static body_temp_t bodyTemperatureDegCelsiusToWire(double degCelsius) { return static_cast<body_temp_t>(degCelsius * 10.0); }
};

#endif // SCALING_H
