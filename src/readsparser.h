/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef READSPARSER_H
#define READSPARSER_H

#include <QString>
#include <QVector>

#include <mosquitto.h>

#include "definitions.h"
#include "mosimpaqt_export.h"

using namespace MoSimPa;

class QJsonArray;

class MOSIMPAQT_EXPORT ReadsParser
{
public:
    struct SpO2Values {
        time_t time;
        spo2_t spO2;
    };

    struct HeartRateValues {
        time_t time;
        heart_rate_t heartR;
    };

    struct BloodPressureValues {
        time_t time;
        blood_pressure_t sys;
        blood_pressure_t dias;
    };

    struct BodyTempValues {
        time_t time;
        body_temp_t bodyTemp;
    };

    ReadsParser();
    static QString topic() { return QStringLiteral("reads/#"); }

    [[nodiscard]] bool parseMessage(const mosquitto_message *msg);

    QString mac() const { return _mac; }

    int spO2ValuesSize() const { return _spo2Values.size(); }
    int heartRateValuesSize() const { return _heartRateValues.size(); }
    int bloodPressureValuesSize() const { return _bloodPressureValues.size(); }
    int bodyTempValuesSize() const { return _bodyTempValues.size(); }

    QVector<SpO2Values> spO2Values() const { return _spo2Values; }
    QVector<HeartRateValues> heartRateValues() const { return _heartRateValues; }
    QVector<BloodPressureValues> bloodPressureValues() const { return _bloodPressureValues; }
    QVector<BodyTempValues> bodyTempValues() const { return _bodyTempValues; }

private:
    void parseSpO2(const QJsonArray & spo2Object);
    void parseBloodPressure(const QJsonArray & bloodPObject);
    void parseHeartRate(const QJsonArray & heartRateObject);
    void parseBodyTemp(const QJsonArray & bodyTempObject);

    QString _mac;
    QVector<SpO2Values> _spo2Values;
    QVector<HeartRateValues> _heartRateValues;
    QVector<BloodPressureValues> _bloodPressureValues;
    QVector<BodyTempValues> _bodyTempValues;
};

#endif // READSPARSER_H
