/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef MOSQUITTOCLIENT_H
#define MOSQUITTOCLIENT_H

#include <QObject>
#include <QString>
#include <QByteArray>
#include <QVector>
#include <QMutex>

#include <mosquitto.h>
#include "mosimpaqt_export.h"

class MOSIMPAQT_EXPORT MosquittoClient : public QObject
{
    Q_OBJECT
public:
    explicit MosquittoClient(bool logEnabled = true, QObject *parent = nullptr);
    ~MosquittoClient();

    int tlsSet(const QString & caFile, const QString & caPath = QString(),
               const QString & certFile = QString(), const QString & keyFile = QString());
    int connect(const QString & host, const int port=1883, const int keepAlive=30);
    int publish(const QString topic, const QByteArray payload, int qos, bool retain);
    int subscribe(const QString subscriptionPattern, const int qos);
    int unsubscribe(const QString & unsubscriptionPattern);
    void setEnableLogs(bool enable) { mLogEnabled = enable; }

    QVector<struct mosquitto_message*> messages();

signals:
    void connectionTried(int result);
    void errorWhileLooping(int error);
    void messageReceived();

private slots:
    void readData();

private:
    static void trampLogCallback(struct mosquitto *mosq, void *userdata, int level, const char *str);
    static void trampConnectCallback(struct mosquitto *mosq, void *userdata, int result);
    static void trampMessageCallback(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *msg);
    static void trampSubscribeCallback(struct mosquitto *mosq, void *userdata, int mId, int qosCount, const int * grantedQos);

    void logCallback(struct mosquitto *mosq, void *userdata, int level, const char *str);
    void connectCallback(struct mosquitto *mosq, void *userdata, int result);
    void messageCallback(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *msg);
    void subscribeCallback(struct mosquitto *mosq, void *userdata, int mId, int qosCount, const int * grantedQos);

    struct mosquitto * mMosq;

    QMutex mMsgsMutex;
    QVector<struct mosquitto_message *> mMsgs;
    bool mLogEnabled;
    bool mIsRunning;
};

#endif // MOSQUITTOCLIENT_H
